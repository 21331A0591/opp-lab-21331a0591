import java.util.Scanner;

public class Java4 {
    public static void main(String[] args)
    {
        
        int ans=0;
        Scanner input=new Scanner(System.in);
        System.out.println("enter two numbers :");
        int a=input.nextInt();
        int b=input.nextInt();
        System.out.println("enter any operator");
        char ch=input.next().charAt(0);
        if (ch == '+')
        {
            ans=a+b;
        }
        else if(ch == '-')
        {
            ans=a-b;
        }
       else if(ch == '*')
        {
            ans=a*b;
        }
        else if(ch == '/')
        {
            if(b==0)
            {
                System.out.println("error");
            }
            else{
                ans=a/b;
            }
        }
        else if(ch == '%')
        {
            ans=a%b;
        }
        System.out.println(ans);
        
    }
    
}
